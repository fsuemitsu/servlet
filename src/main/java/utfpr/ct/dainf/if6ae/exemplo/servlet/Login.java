/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class Login extends HttpServlet {
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
        String perfil = request.getParameter("perfil");
        
        if(Validate.checkUser(login, senha, perfil))
        {
            String queryString = "sucesso?login=";
            queryString = queryString.concat(login);
            queryString = queryString.concat("&perfil=");
            queryString = queryString.concat(perfil);
            request.getRequestDispatcher(queryString).forward(request, response);
        }
        else
        {
           request.getRequestDispatcher("erro.xhtml").include(request, response);
        }
    }  
}