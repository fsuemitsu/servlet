/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet(name = "sucesso", urlPatterns = {"/sucesso"})
public class Sucesso extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
                    + "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Bem vindo</title>");
            out.println("</head>");
            out.println("<body>");
            String login = request.getParameter("login");
            String perfil = request.getParameter("perfil");
            if (perfil.trim().equalsIgnoreCase("1")) {
                out.println("<h1>Acesso a Cliente " + login + " permitido</h1>");
            }
            if (perfil.trim().equalsIgnoreCase("2")) {
                out.println("<h1>Acesso a Gerente " + login + " permitido</h1>");
            }
            if (perfil.trim().equalsIgnoreCase("3")) {
                out.println("<h1>Acesso a Administrador " + login + " permitido</h1>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }
}
